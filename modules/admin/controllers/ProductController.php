<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 22.9.2015 �.
 * Time: 21:22
 */

namespace app\modules\admin\controllers;

use app\models\Category;
use app\models\Product;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

use Yii;
use yii\web\UploadedFile;


class ProductController extends Controller
{
    public function actionIndex(){

        $request = Yii::$app->request;

        $id = 0;

        if($request->post('category_id') !== null){
            $id = $request->post('category_id');
        }

        $dataProvider = $this->_getDataProvider($id);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category_id' => $id
        ]);
    }

    public function actionNew(){

        $request = Yii::$app->request;

        $model = new Product();

        if($request->isPost){
            $file = UploadedFile::getInstanceByName('Product[avatar]');
            $newName = 'image' . time() . '.' . $file->extension;
            $file->saveAs(Yii::$app->basePath . '/web/images/' . $newName);

            if($model->load($request->post()) && $model->validate()){
                $model->avatar = $newName;
                if($model->save()){
                    return $this->redirect('index');
                }
            }
        }

        if($request->get('category') !== null){
            $categoryId = $request->get('category');

            $categoryModel = Category::findOne($categoryId);

            if($categoryModel instanceof Category){

                return $this->render('new', [
                    'categoryModel' => $categoryModel,
                    'model' => $model
                ]);
            }
        }

        return $this->redirect('admin');
    }

    public function actionUpdate(){
        $request = Yii::$app->request;

        $id = $request->get('id', null);

        if($id === null){
            return $this->redirect('admin');
        }

        $model = Product::findOne($id);

        /**
         * @var $model Product
         */

        if(($model instanceof Product) === false){
            return $this->redirect('admin');
        }

        $categoryModel = Category::findOne($model->category_id);

        if(($categoryModel instanceof Category) === false){
            return $this->redirect('admin');
        }

        if($request->isPost){
            $file = UploadedFile::getInstanceByName('Product[avatar]');
            $newName = 'image' . time() . '.' . $file->extension;
            $file->saveAs(Yii::$app->basePath . '/web/images/' . $newName);

            if($model->load($request->post()) && $model->validate()){
                $model->avatar = $newName;

                if($model->save()){
                    return $this->redirect(['index']);
                }
            }

        }

        return $this->render('new', [
            'model' => $model,
            'categoryModel' => $categoryModel
        ]);
    }

    public function actionDelete(){
        $request = Yii::$app->request;

        $id = $request->get('id', null);

        if($id === null){
            return $this->redirect('index');
        }

        $model = Product::findOne($id);

        if(($model instanceof Product) === false){
            return $this->redirect('index');
        }

        if($model->delete()){
            return $this->redirect('index');
        }
    }

    private function _getDataProvider($id = 0){

        if($id === 0){
            $dataProvider = new ActiveDataProvider([
                'query' => Product::find()
            ]);

            return $dataProvider;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()->where('category_id = :id', [
                ':id' => $id
            ])
        ]);

        return $dataProvider;

    }
}