<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 22.9.2015 �.
 * Time: 21:18
 */

namespace app\modules\admin\controllers;

use app\models\Category;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use Yii;



class CategoryController extends Controller
{
    public function actionIndex(){

        $dataProvider = $this->_getDataProvider();

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionNew(){

        $request = Yii::$app->request;
        $model = new Category();

        if($request->isPost){
            if($model->load($request->post()) && $model->save()){
                return $this->redirect(['/admin/category']);
            }
        }

        return $this->render('new', [
            'model' => $model
        ]);
    }

    public function actionUpdate(){
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = Category::findOne([
            'id' => $id
        ]);

        if($request->isPost){
            if($model->load($request->post()) && $model->save()){
                return $this->redirect(['/admin/category']);
            }
        }

        return $this->render('new', [
            'model' => $model
        ]);
    }

    public function actionDelete(){
        $request = Yii::$app->request;

        $id = $request->get('id');

        $model = Category::findOne($id);

        if($model->delete()){
            return $this->redirect(['/admin/category']);
        }
    }

    private function _getDataProvider(){
        $dataProvider = new ActiveDataProvider([
            'query' => Category::find()
        ]);

        return $dataProvider;
    }


}