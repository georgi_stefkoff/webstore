<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

    $form = ActiveForm::begin([
        'id' => 'new-category-form',
        'options' => [
            'class' => 'form-horizontal'
        ]
    ]);

    echo $form->field($model, 'name');
    echo $form->field($model, 'description')->textarea();

    echo Html::submitButton('Submit', [
        'class' => 'btn btn-primary'
    ]);

    ActiveForm::end();