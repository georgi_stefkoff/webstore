<?php

use \yii\grid\GridView;
use yii\helpers\Html;

echo Html::a('Add new', ['/admin/category/new'], [
    'class' => 'btn btn-primary'
]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        'description',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}&nbsp;&nbsp;{delete}',
        ]
    ]
])


?>