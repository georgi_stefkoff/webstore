<?php

use yii\helpers\Html;
use app\models\Product;
use app\models\Category;
use yii\grid\GridView;
use yii\helpers\Url;

echo Html::beginForm();

echo Html::dropDownList('category_id', $category_id, Category::getCategoriesList(), [
    'prompt' => 'Select...'
]);

echo Html::endForm();

echo "<br>";

if($category_id !== 0){
    echo Html::a('Add new', Url::toRoute('product/new?category=' . $category_id), [
        'class' => 'btn btn-primary',
        'id' => 'new-product',
    ]);
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'header' => 'Avatar',
//            'format' => ['image', ['width' => 150, 'height' => 170]],
            'format' => 'raw',
            'value' => function($data){
                return $data->getAvatar();
            }
        ],
        'name',
        'description',
        'price',
        [
            'header' => 'Category',
            'format' => 'raw',
            'value' => function($data){
                return $data->getCategoryName();
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}&nbsp;&nbsp;{delete}',
        ]
    ]
]);

?>
<script type="text/javascript">
    $(function (){
        $(document).delegate('select[name=category_id]', 'change', function() {
            $(this).closest('form').submit();
        });
    });
</script>