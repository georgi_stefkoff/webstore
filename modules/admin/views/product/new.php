<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 22.9.2015 �.
 * Time: 23:31
 *
 * @var $model Product
 * @var $categoryModel Category
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\Category;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'id' => 'form-product',
    'options' => [
        'class' => 'form-horizontal',
        'enctype'=>'multipart/form-data'
    ]
]);

$uplodedImage = (isset($model->avatar)) ? Html::img('@web/images/' . $model->avatar, [
    'class' => 'file-preview-image',
    'alt' => $model->name,
    'title' => $model->name
]) : '';

echo $form->field($model, 'avatar')->widget(FileInput::className(), [
    'options' => [
        'accept' => 'image/*',
        'multiple' => false
    ],
    'pluginOptions' => [
        'initialPreview' => $uplodedImage,
        'showUpload' => false
    ]
]);

echo $form->field($model, 'name');

echo $form->field($model, 'description');

echo "<div class='form-group'>";
echo Html::label('Category', 'product-category');
echo Html::dropDownList('Product[category_id]', $categoryModel->id , Category::getCategoriesList(), [
    'prompt' => 'Select...',
    'class' => 'form-control',
    'product-category'
]);
echo "</div>";

echo $form->field($model, 'price');

echo Html::submitButton('Submit', [
    'class' => 'btn btn-primary'
]);

ActiveForm::end();