<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use branchonline\lightbox\Lightbox;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property string $avatar
 *
 * @property Category $category
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'description', 'price'], 'required'],
            [['category_id'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 32],
            [['avatar'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'avatar' => 'Avatar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getCategoryName(){
        $categoryId = $this->category_id;

        $categoryModel = Category::findOne($categoryId);

        return $categoryModel->name;
    }

    public function getAvatar(){
        $fileName = $this->avatar;

        return Html::img('@web/images/' . $fileName, [
            'width' => 150,
            'height' => 170,
            'alt' => $this->name
        ]);

//        return "<a href='" . Yii::getAlias('@web/images/' . $this->avatar)  . "' data-lightbox='" . time() . "'>" . $this->avatar . "</a>'";

    }

    public static function getProductList(){
        $products = self::find()->all();

        return ArrayHelper::map($products, 'id', 'name');
    }
}
